const express = require('express');
const logger = require('morgan');
const Pinsrouter = require('./routes/pins');
const pins = require('./models/Pins');
const http = require('http');
const axios = require('axios');
const request = require('request');
const app = express();
var requestPromise = require('request-promise-native');

app.use(logger('dev'))
app.use(express.json())
app.use('/api', Pinsrouter.router);
app.set('port', 3000);

describe('Testing Router', () => {
  let server;

  beforeAll(() => {
    server = http.createServer(app);
    server.listen(3000);
  })

  afterAll(() => {
    server.close();
  });

  describe('GET', () => {
    //Get status 200
    it('200 and find pin', done => {
      const data = [{id: 1}];
      spyOn(pins, 'find').and.callFake(callBack => {
        callBack(false, data);
      });

      request.get('http://localhost:3000/api', (error, response, body) => {
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.body)).toEqual([{id: 1}]);
        done();
      })
    });

    // Get status 500
    it('500',done => {
      const data = [{id: 1}];
      spyOn(pins, 'find').and.callFake(callBack => {
        callBack(true, data);
      });

      request.get('http://localhost:3000/api', (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      })
    });

    it('200 and findById',done => {
      spyOn(pins,'findById').and.callFake((id, callBack) => {
        callBack(false, id);
      });

      request.get('http://localhost:3000/api/1', (error, response, body) => {
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.body)).toEqual("1");
        done();
      });
    })

    it('500 and findById',done => {
      spyOn(pins,'findById').and.callFake((id, callBack) => {
        callBack(true, id);
      });

      request.get('http://localhost:3000/api/1', (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      });
    })

  })

  describe('POST', () => {
    // status 200
    it('status 200',done =>{
      const post = [{
        title: 'Platzi',
        author: 'Platzi',
        description: 'Platzi rules',
        percentage: 0,
        tags: [],
        assets: [{
          title: 'Platzi',
          description: 'description',
          readed: false,
          url: 'http://platzi.com'
        }]
      }];
        spyOn(pins, 'create').and.callFake((pin, callBack) => {
          callBack(false, {})
        });

        spyOn(requestPromise, 'get').and.returnValue(
          Promise.resolve('<title>Platzi</title><meta name="description" content="Platzi rules">')
        );

        const assets = [{url: 'http:/platzi.com'}];

        axios.post('http://localhost:3000/api',
        {title: 'title', author: 'author', description: 'description', assets})
        .then(
          res => {
            expect(res.status).toBe(200);
            done();
        });
    });

    it('status 500', done => {
      spyOn(pins,'create').and.callFake((pin, callBack) => {
        callBack(true, pin);
      });

      const assets = [{url: 'http:/platzi.com'}];

      request.post('http://localhost:3000/api', {
        json:{
          title: "title",
          author: "author",
          description: "description",
          assets
        }
      }, (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      });
    });

    it('200 PDF', done => {
      spyOn(pins,'create').and.callFake((Pins, callBack) => {
        callBack(false, {});
      });

      const assets = [{url: 'http:/platzi.pdf'}];

      axios.post('http://localhost:3000/api',
      {title: 'title', author: 'author', description: 'description', assets})
      .then(res => {
        expect(res.status).toBe(200);
        done();
      });
    });

    it('status 500 pdf', done => {
      spyOn(pins, 'create').and.callFake((Pins, callBack) => {
        callBack(true, Pins);
      });

      const assets = [{url: 'http:/platzi.pdf'}];

      request.post('http://localhost:3000/api',{
        json:{
          title: "title",
          author: "author",
          description: "description",
          assets
        }
      }, (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      });
    });

  });

  describe('DELETE', () => {
    it('status 200', done => {
      spyOn(pins, 'findByIdAndRemove').and.callFake((id, body, callBack) => {
        callBack(false, body, id);
      });

      axios.delete('http://localhost:3000/api/1')
      .then(
        res => {
         expect(res.status).toBe(200);
         done();
        });
    });

    it('status 500', done => {
      spyOn(pins,'findByIdAndRemove').and.callFake((id, body, callBack) => {
        callBack(true, body, id);
      });

      request.delete('http://localhost:3000/api/1', (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      });
    });
  });

  describe('PUT',() => {
    it('status 200', done => {
      spyOn(pins, 'findByIdAndUpdate').and.callFake((id, body, callBack) => {
        callBack(false, body, id);
      });

      axios.put('http://localhost:3000/api/1')
      .then(
        res => {
          expect(res.status).toBe(200);
          done();
      });
    });

    it('status 500', done => {
      spyOn(pins, 'findByIdAndUpdate').and.callFake((id, body, callBack) => {
        callBack(true, body, id);
      });

      request.get('http://localhost:3000/api/1', (error, response, body) => {
        expect(response.statusCode).toBe(500);
        done();
      });
    });
  })

});
