import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinsComponent } from './pins.component';
import { RepositoryService } from '../../services/repository.service';
import { MatSnackBar } from '@angular/material';
import { PinsService } from './pins.service';
import { ReactiveFormsModule } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { PINS } from '../../services/mocks/pins';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

class RepositoryServiceStub {
  observer = new Subject();

  getPins() {
    return this.observer;
  }

  resolverPins() {
    this.observer.next(JSON.parse(JSON.stringify(PINS)));
  }

  updatePin() {
    return of(true);
  }
}

class MatSnackBarStub {
  open() {}
}

class PinsServiceStub {
  observer = new Subject();
  $actionObserver = this.observer.asObservable();

  public resolve(action) {
    return this.observer.next(action);
  }
}

fdescribe('PinsComponent', () => {
  let component: PinsComponent;
  let fixture: ComponentFixture<PinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinsComponent ],
      providers: [
        { provide: RepositoryService, useClass: RepositoryServiceStub },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        { provide: PinsService, useClass: PinsServiceStub }
      ],
      imports: [ReactiveFormsModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('When new page is open', () => {
    const open = spyOn(window, 'open');

    component.openUrl('https://platzi.com');

    expect(open).toHaveBeenCalledWith('https://platzi.com', '_blank');
  });

  it('When update progress', () => {
    const pin = PINS[0];
    const object = [{_id: 1,
      title: 'Learning path 1',
      author: 'Johan palma',
      description: 'Description',
      percentage: 0,
      tags: [
        'Javascript', 'Code', 'Video'
      ],
      assets: pin
    }];
    component.pins = PINS;
    const updatePin = spyOn((<any>component).repository, 'updatePin').and.returnValue(of(true));
    const open = spyOn((<any>component).snackBar, 'open');
    const pinService = TestBed.get(PinsService);
    const repositoryService = TestBed.get(RepositoryService);

    pinService.resolve('save');
    repositoryService.updatePin(object);

    expect(open).toHaveBeenCalled();
    expect(updatePin).toHaveBeenCalledWith(object);
  });
});
