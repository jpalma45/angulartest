import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';

fdescribe('ApiService', () => {
  let service: ApiService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [ApiService],
    imports: [HttpClientTestingModule]
  }));

  beforeEach(() => {
    injector = getTestBed();
    service = TestBed.get(ApiService);
    httpMock = injector.get(HttpTestingController);
  });

  // Para cerrar todas las pruebas despues que se ejecute todo
  afterAll(() => {
    injector = null;
    service = null;
    httpMock = null;
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  describe('GET', () => {
    it('Should execute GET', () => {
      const result = 'testing';

      service.get('/test').subscribe(response => {
        expect(response).toBe(result);
      });

      const req = httpMock.expectOne(environment.apiEndpoint + '/test');
      expect(req.request.method).toBe('GET');
      req.flush(result);
    });

    it('Should execute GET with headers', () => {
      const result = 'testing';
      const headers = new HttpHeaders().set('platzi-headers', 'johan-palma');

      service.get('/test', headers).subscribe(response => {
        expect(response).toBe(result);
      });

      const req = httpMock.expectOne(environment.apiEndpoint + '/test');
      expect(req.request.headers.get('platzi-headers')).toBe('johan-palma');
      expect(req.request.method).toBe('GET');
      req.flush(result);
    });
  });

  describe('POST', () => {
    it('Should execute POST', () => {
      const result = 'testing';

      service.post('/test', {}).subscribe(response => {
        expect(response).toBe(result);
      });

      const req = httpMock.expectOne(environment.apiEndpoint + '/test');
      expect(req.request.method).toBe('POST');
      req.flush(result);
    });
  });

  describe('PUT', () => {
    it('Should execute PUT', () => {
      const result = 'testing';

      service.put('/test', {}).subscribe();

      const req = httpMock.expectOne(environment.apiEndpoint + '/test');
      expect(req.request.method).toBe('PUT');
      req.flush(result);
    });
  });

  describe('DELETE', () => {
    it('Should execute DELETE', () => {
      const result = 'testing';

      service.delete('/test').subscribe();

      const req = httpMock.expectOne(environment.apiEndpoint + '/test');
      expect(req.request.method).toBe('DELETE');
      req.flush(result);
    });
  });
});
