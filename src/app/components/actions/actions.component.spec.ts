import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsComponent } from './actions.component';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';
import { of } from 'rxjs';
import { PinsService } from '../pins/pins.service';

class PinsComponentStub {
  resolveActionObserver() {
    return of(true);
  }
}

class MatBottomSheetRefStub {
  dismiss() {

  }
}

fdescribe('ActionsComponent', () => {
  let component: ActionsComponent;
  let fixture: ComponentFixture<ActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsComponent ],
      providers: [
        { provide: PinsService, useClass: PinsComponentStub },
        { provide: MatBottomSheetRef, useClass: MatBottomSheetRefStub }
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should openLink', () => {
    const event = new MouseEvent('click');

    const eventCall = spyOn(event, 'preventDefault').and.callFake(() => {});

    const dismiss = spyOn((<any>component).bottomSheetRef, 'dismiss');

    const resolveActionObserve = spyOn((<any>component).pinsService, 'resolveActionObserver');

    component.openLink(event, '');
    expect(eventCall).toHaveBeenCalled();
    expect(dismiss).toHaveBeenCalled();
    expect(resolveActionObserve).toHaveBeenCalled();
  });
});
