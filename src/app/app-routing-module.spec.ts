import { routes } from './app-routing.module';
import { PinsComponent } from './components/pins/pins.component';

fdescribe('App Routing', () => {
  // se ejecuta antes de todas pruebas
  beforeAll(() => {
    // console.log("beforeAll");
  });

  // se ejecuta antes de cada pruebas
  beforeEach(() => {
    // console.log("beforeEach");
  });

  // se ejecuta despues de todas pruebas
  afterAll(() => {
    // console.log("afterAll");
  });

  // se ejecuta despues de terminar la ejecuecion de una prueba
  afterEach(() => {
    // console.log("afterEach");
  });


  it('Should have app as path', () => {
    expect(routes[0].path).toBe('app');
  });

  it('Should match the children', () => {
    expect(routes[0].children).toContain({
      path: 'pins',
      component: PinsComponent
    });
  });

});
